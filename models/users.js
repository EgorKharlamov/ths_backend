'use strict';
module.exports = (sequelize, DataTypes) => {
  const Users = sequelize.define('Users', {
    phoneNumber: DataTypes.STRING,
    password: DataTypes.STRING
  }, {});
  return Users;
};