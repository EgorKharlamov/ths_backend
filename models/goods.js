'use strict';
module.exports = (sequelize, DataTypes) => {
  const Goods = sequelize.define('Goods', {
    name: DataTypes.STRING,
    price: DataTypes.FLOAT,
    info: DataTypes.STRING
  }, {});
  return Goods;
};