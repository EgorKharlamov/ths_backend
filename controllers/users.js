const Users = require("../models").Users;
const jwt = require("jsonwebtoken");

let currentToken = ''

module.exports = {
  list(req, res) {
    return Users.findAll()
      .then(users => res.status(200).send(users))
      .catch(error => {
        res.status(400).send(error);
      });
  },

  getById(req, res) {
    return Users.findByPk(req.params.id)
      .then(user => {
        if (!user) {
          return res.status(404).send({
            message: "User Not Found"
          });
        }
        return res.status(200).send(user);
      })
      .catch(error => res.status(400).send(error));
  },

  async add(req, res) {
    try {
      const userExists = await Users.findOne({
        where: { phoneNumber: req.body.phone }
      });

      if (userExists)
        return res.status(201).send({ phoneErr: "user already exists" });
      if (!req.body.phone)
        return res.status(201).send({ phoneErr: "enter the phone number" });
      if (req.body.phone.length < 11)
        return res.status(201).send({ phoneErr: "incorrect phone number" });
      if (!req.body.password)
        return res.status(201).send({ passwordErr: "enter the password" });
      if (req.body.password.length < 8)
        return res
          .status(201)
          .send({ passwordErr: "minimal password length is 8 letter" });

      return Users.create({
        phoneNumber: req.body.phone,
        password: req.body.password
      }).then(user => res.status(201).send(user));
    } catch (e) {
      return res.status(400).send(e);
    }
  },

  async signIn(req, res) {
    console.log(`
    ${Object.keys(req.body)}
    `)
    try {
      const userExists = await Users.findOne({
        where: { phoneNumber: req.body.phone }
      });
      if (!req.body.phone)
        return res.status(201).send({ phoneErr: "enter the phone number" });
        else if (!userExists)
          return res.status(201).send({ phoneErr: "no such user" });
      else if (!req.body.password.length)
        return res.status(201).send({ passwordErr: "enter the password" });
      else if (req.body.password !== userExists.dataValues.password)
        return res.status(201).send({ passwordErr: "try another password" });

      jwt.sign({ userExists }, "secretKey", (err, token) => {
        currentToken = token
        res.json({
          token,
          phoneNumber: userExists.dataValues.phoneNumber
        });
      });
    } catch (e) {
      return res.status(400).send(e);
    }
  },

  // update(req, res) {
  //   return Users.findById(req.params.id, {
  //     include: [
  //       {
  //         model: Users,
  //         as: "users"
  //       }
  //     ]
  //   })
  //     .then(user => {
  //       if (!user) {
  //         return res.status(404).send({
  //           message: "User Not Found"
  //         });
  //       }
  //       return user
  //         .update({
  //           username: req.body.username || user.username,
  //           password: req.body.password || user.password
  //         })
  //         .then(() => res.status(200).send(user))
  //         .catch(error => res.status(400).send(error));
  //     })
  //     .catch(error => res.status(400).send(error));
  // },

  // delete(req, res) {
  //   return Users.findById(req.params.id)
  //     .then(user => {
  //       if (!user) {
  //         return res.status(400).send({
  //           message: "User Not Found"
  //         });
  //       }
  //       return user
  //         .destroy()
  //         .then(() => res.status(204).send())
  //         .catch(error => res.status(400).send(error));
  //     })
  //     .catch(error => res.status(400).send(error));
  // }
};

// export {currentToken}