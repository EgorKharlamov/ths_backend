const users = require("./users");
const goods = require("./goods");

module.exports = {
  users,
  goods,
};
