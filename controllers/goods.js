const Goods = require("../models").Goods;

module.exports = {
  list(req, res) {
    return Goods.findAll()
      .then(goods => res.status(200).send(goods))
      .catch(error => {
        res.status(400).send(error);
      });
  }
};
