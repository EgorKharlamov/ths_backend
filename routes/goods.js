var express = require("express");
var router = express.Router();
const jwt = require("jsonwebtoken");

/* Goods Router */
const goodsController = require("../controllers").goods;
router.get("/", verifyToken, goodsController.list);

function verifyToken(req, res, next) {
  const bearerHeader = req.headers["authorization"];
  if (bearerHeader) {
    const bearer = bearerHeader.split(" ");
    const bearerToken = bearer[1];
    req.token = bearerToken;
    try {
      jwt.verify(bearerToken, 'secretKey')
    } catch (e) {
      return res.status(200).send('invalid token')
    }
    next();
  } else {
    return res.status(403).send("oh shit");
  }
}
module.exports = router;
