var express = require('express');
var router = express.Router();

const usersController = require("../controllers").users;

/* Users Router */
router.get("/", usersController.list);
router.get("/:id", usersController.getById);
router.post("/signUp", usersController.add);
router.post("/signIn", usersController.signIn);

// router.put('/api/users/:id', usersController.update);
// router.delete('/api/users/:id', usersController.delete);

module.exports = router;
